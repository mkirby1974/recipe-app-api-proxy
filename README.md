# Recipe app api proxy

NGINX proxy app for our recipe app api

## Usage

### ENvironment Variables

 * `LISTEN_PORT` - Port to listen on (default: `8000`)
 * `APP_HOST` - host name of the app to forward requests to (default: `app`)
 * `APP_PORT` - port of the app tp forward requests to (default: `9000`)
 